import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget {
  const AboutUs({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        // backgroundColor: Colo,
        title: const Text(
          "About Us",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: const Padding(
        padding: EdgeInsets.all(12.0),
        child: Text(
            "Bienvenue sur notre plateforme de commerce électronique multivendeur ! Chez nous, nous croyons en la puissance de la collaboration entre vendeurs passionnés et clients avides de découvertes. Notre objectif est de créer un espace dynamique où les vendeurs peuvent présenter leurs produits uniques et les clients peuvent trouver des articles qui correspondent parfaitement à leurs besoins.Notre mission est de fournir une expérience d'achat en ligne exceptionnelle, en mettant l'accent sur la diversité des produits, la qualité et la satisfaction des clients. En rassemblant des vendeurs talentueux de diverses niches, nous offrons une gamme étendue de produits allant des vêtements à la maison, de la technologie aux produits artisanaux.Nous avons créé une plateforme conviviale et intuitive qui facilite la recherche, la comparaison et l'achat de produits. Vous pouvez naviguer à travers les différentes catégories, explorer les magasins des vendeurs et consulter les descriptions détaillées des produits. De plus, notre système de notation et de commentaires vous permet de prendre des décisions éclairées en vous basant sur les expériences d'autres clients.Nous accordons une grande importance à la confiance et à la sécurité de nos utilisateurs. Nous avons mis en place des mesures de protection des données et des transactions pour garantir une expérience sécurisée. De plus, notre service clientèle est disponible pour répondre à toutes vos questions, résoudre les problèmes éventuels et vous offrir une assistance personnalisée.Nous sommes ravis de vous accompagner dans votre parcours d'achat en ligne et de vous connecter avec des vendeurs talentueux du monde entier. Explorez notre plateforme, découvrez de nouveaux produits passionnants et profitez d'une expérience de shopping unique. Merci de nous faire confiance pour vos besoins en matière d'e-commerce multivendeur."),
      ),
    );
  }
}
