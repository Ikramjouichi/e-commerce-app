import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:test_flutter/seller/Controller/HomeContoller.dart';
import 'package:test_flutter/seller/Consts/const.dart';
import 'package:test_flutter/seller/models/category_model.dart';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:velocity_x/velocity_x.dart';


class ShopController  extends GetxController{

  var isloading = false.obs; 

  var shopnameController = TextEditingController();
  var shopdescriptionController = TextEditingController();
  var shopadressController = TextEditingController();
  var shopwebController = TextEditingController();
  var shopmobileController = TextEditingController();


  createShop(context)async{
    var store = FirebaseFirestore.instance.collection('shop').doc();
    await store.set({
      'Description':shopdescriptionController.text,
      'name':shopnameController.text,
      'adress':shopadressController.text,
      'shopMobile':shopmobileController.text,
      'shopWebsite':shopwebController.text,
      'seller':Get.find<HomeContoller>().username,
      
    });
    isloading(false);
    VxToast.show(context, msg: "Shop created");
  }
}