import 'package:flutter/material.dart';
import 'package:test_flutter/seller/Consts/colors.dart';
import 'package:test_flutter/seller/widgets/normal_text.dart';

class ConnectAdmin extends StatelessWidget {
  const ConnectAdmin({Key? key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contact Admin'),
      ),
      body: Container(
        height: 230,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              spreadRadius: 2,
              blurRadius: 5,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              boldText(
                text: 'To contact the administrator, please use the following information:',
                color: red,
                size: 20.0,
              ),
              SizedBox(height: 16.0),
              normalText(
                text: 'Email Address: admin@example.com',
                color: Colors.black,
                size: 14.0,
              ),
              SizedBox(height: 8.0),
              normalText(
                text: 'Phone Number: +123456789',
                color: Colors.black,
                size: 14.0,
              ),
              SizedBox(height: 8.0),
              normalText(
                text: 'Availability Hours: Monday to Friday, 9:00 AM to 5:00 PM',
                color: Colors.black,
                size: 14.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
