import 'package:get/get.dart';
import 'package:test_flutter/seller/Consts/const.dart';
import 'package:test_flutter/seller/Controller/profile_controller.dart';
import 'package:test_flutter/seller/Controller/shop_controller.dart';
import 'package:test_flutter/seller/widgets/custom_textField.dart';
import 'package:test_flutter/seller/widgets/normal_text.dart';
import 'package:velocity_x/velocity_x.dart';

class ShopSettings extends StatefulWidget {
  const ShopSettings({super.key});

  @override
  State<ShopSettings> createState() => _ShopSettingsState();
}

class _ShopSettingsState extends State<ShopSettings> {
  @override
  Widget build(BuildContext context) {
    var controller = Get.put(ShopController());
    return Scaffold(
      backgroundColor: purpleColor,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: boldText(text: shopSettings ,color: whiteColor,size: 16.0),
        actions: [
          TextButton(
            onPressed: ()async{
              await controller.createShop(context);
              Get.to(() => ProfileController());
            }, 
            child:normalText(text: save , color: purpleColor) )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            customTextField(label: shopname,hint: nameHint , controller:  controller.shopnameController),
            10.heightBox,
            customTextField(label: adress,hint: shopAddressHint ,controller:  controller.shopadressController),
            10.heightBox,
            customTextField(label: mobile,hint: shopMobileHint,controller:  controller.shopmobileController),
            10.heightBox,
            customTextField(label: website,hint: shopWebsiteHint,controller:  controller.shopwebController),
            10.heightBox,
            customTextField(label:description,hint: shopDescHint,isDesc: true , controller:  controller.shopdescriptionController),
            10.heightBox,
          ],
        ),
      ),
    );
  }
}