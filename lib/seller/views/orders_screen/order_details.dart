import 'package:flutter/material.dart';
import 'package:test_flutter/seller/Consts/const.dart';
import 'package:test_flutter/seller/Controller/orders_controller.dart';
import 'package:test_flutter/seller/views/messages_screen/chat_screen.dart';
import 'package:test_flutter/seller/views/orders_screen/components/order_place.dart';
import 'package:test_flutter/seller/widgets/custom_textField.dart';
import 'package:test_flutter/seller/widgets/normal_text.dart';
import 'package:test_flutter/seller/widgets/our_button.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart' as intl ;


class OrderDetails extends StatefulWidget {
  final dynamic data;
  const OrderDetails({super.key,this.data});

  @override
  State<OrderDetails> createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  var controller =  Get.find<OrdersController>();

  void initState(){
    super.initState();
    //controller.getOrders(widget.data);
  }
  @override
  Widget build(BuildContext context) {
    print("${widget.data['products']}");
    var productsData = widget.data['products'][0]; // Récupérer les données du premier élément dans la liste

    var imageUrl = productsData['image']; // Accéder à l'attribut 'image'
    var price = productsData['price']; // Accéder à l'attribut 'price'
    var qty = productsData['qty']; // Accéder à l'attribut 'qty'
    var name = productsData['name']; // Accéder à l'attribut 'name'
    var description = productsData['description']; // Accéder à l'attribut 'description'
    var id = productsData['id']; // Accéder à l'attribut 'id'
    var isFavourite = productsData['isFavourite']; // Accéder à l'attribut 'isFavourite'

    
    return  Scaffold(
        appBar: AppBar(
          leading:IconButton(
            icon: Icon(Icons.arrow_back,color: fontGrey),
            onPressed: () {
              Get.back();
            },
          ),
          title: boldText(text:"Order details",size: 16.0,color: fontGrey),
        ),
        bottomNavigationBar: Visibility(
          visible:true,//controller.confirmed.value,
          child: SizedBox(
            height: 60,
            width: context.screenWidth,
            child:ourButton(color: green,onPress: (){},title: "Confirm Order") 
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            // child: Column(
            //   children: [
            //     //boldText(text:"${widget.data['products']}",color: purpleColor),
            //     Column(
            //       children: [
            //         boldText(text: "Image: ${productsData['image']}", color: purpleColor),
            //         Image.network("${productsData['image']}"),
            //         boldText(text: "Price: ${productsData['price']}", color: purpleColor),
            //         boldText(text: "Qty: ${productsData['qty']}", color: purpleColor),
            //         boldText(text: "Name: ${productsData['name']}", color: purpleColor),
            //         boldText(text: "Description: ${productsData['description']}", color: purpleColor),
            //         boldText(text: "ID: ${productsData['id']}", color: purpleColor),
            //         boldText(text: "Is Favourite: ${productsData['isFavourite']}", color: purpleColor),
            //       ],
            //     ),

            //   ],
            // ),
            child: Column(
              children: [
                //order delivery status section
                Visibility(
                  visible: controller.confirmed.value,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      boldText(text: "Order Status",color:purpleColor ,size: 16.0),
                      SwitchListTile(
                        activeColor: green,
                        value: true, 
                        onChanged: (value){},
                        title: boldText(text: "Placed",color: fontGrey),
                      ),
                      SwitchListTile(
                        activeColor: green,
                        value: true, 
                        onChanged: (value){},
                        title: boldText(text: "Confirmed",color: fontGrey),
                      ),
                      SwitchListTile(
                        activeColor: green,
                        value: true, 
                        onChanged: (value){},
                        title: boldText(text: "on Delivery",color: fontGrey),
                      ),
                      SwitchListTile(
                        activeColor: green,
                        value: true, 
                        onChanged: (value){},
                        title: boldText(text: "Delivereded",color: fontGrey),
                      )  
                    ],
                  ).box.padding(const EdgeInsets.all(8)).outerShadowMd.white.border(color: lightGrey).roundedSM.make(),
                ),
    
    
    
              
                //order details section
                Column(
                  children: [
                    Container(
                       width:300,
                      child: orderPlaceDetails(
                        d1: " ${widget.data['totalPrice']}",
                        d2: " ${widget.data['payment']}",
                        title1: "Total Price",
                        title2:"Payment",
                      ),
                    ),
                    
                    boldText(text: "order Id : ${widget.data['orderId']} ",color: purpleColor),
                    boldText(text: "Status : ${widget.data['status']} ",color: purpleColor),
                    /*
                    orderPlaceDetails(
                      d1: "Unpaid",
                      d2: "Order Placed",
                      title1: "Payment Status",
                      title2:"Delivery Status",
                    ),*/
                    Divider(color: Colors.black,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0,vertical: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              //"Shipping Adress".text.fontFamily(semibold).make(),
                              30.heightBox,
                              boldText(text: "Product details",color: purpleColor, size: 22.0),
                              20.heightBox,
                              boldText(text: "Produit Images:",color: red),
                              Image.network("${productsData['image']}", width: 300,),
                              boldText(text: "Price:",color: red),
                              boldText(text: "${productsData['price']}", color: purpleColor),
                              boldText(text: "Quantity:",color: red),
                              boldText(text: "${productsData['qty']}", color: purpleColor),
                              boldText(text: "Product Name:",color: red),
                              boldText(text: "${productsData['name']}", color: purpleColor),
                              boldText(text: "Description:",color: red),
                              Container(
                                width: 260,
                                child: boldText(text: " ${productsData['description']}", color: Colors.green)),
                              boldText(text: "Product ID:",color: red),
                              boldText(text: "${productsData['id']}", color: purpleColor),
                              boldText(text: "Is Favourite:",color: red),
                              boldText(text: " ${productsData['isFavourite']}", color: purpleColor),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ).box.outerShadowMd.white.border(color: lightGrey).roundedSM.make(),
                const Divider(),
                10.heightBox,
                //boldText(text: "Ordered Products ",color: fontGrey ,size: 16.0),
                10.heightBox,
                20.heightBox,
              ],
            ),
          ),
        ) ,
      
    );
  }
}