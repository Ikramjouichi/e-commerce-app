import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_flutter/client/screens/auth_ui/welcome/welcome.dart';
import 'package:test_flutter/seller/Consts/const.dart';
import 'package:test_flutter/seller/views/auth_screen/login_screen.dart';
import 'package:test_flutter/seller/widgets/our_button.dart';
import 'package:velocity_x/velocity_x.dart';

class ChooseUser extends StatelessWidget {
  const ChooseUser({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color:Color.fromARGB(255, 2, 159, 214),
          image: DecorationImage(
            image: AssetImage(shoppingBlue))
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              
              SizedBox(
                height: 540,
              ),
              SizedBox(
                width: context.screenWidth - 100,
                child: ourButton(
                  title: "I am a Seller",
                  onPress: () async{
                    Get.to(() => const LoginScreen());
                  },
                ),
              ),
              SizedBox(
                width: context.screenWidth - 100,
                child: ourButton(
                  title: "I am a Client",
                  onPress: () async{
                    Get.to(() => const Welcome());
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}